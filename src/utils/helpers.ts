/* eslint-disable @typescript-eslint/no-unused-vars */
import { RatesData } from '../App';

function getAvailableList(data: RatesData): string[] {
  return [...new Set(data.map(([baseCurrency]) => baseCurrency))];
}

function findExchangeRate(data: RatesData, fromCurrency: string, toCurrency: string, isBuying: boolean): number | null {
  const [_, __, buyRate, sellRate] =
    data.find(
      ([sourceCurrency, targetCurrency]) => (sourceCurrency === fromCurrency && targetCurrency === toCurrency) || (targetCurrency === fromCurrency && sourceCurrency === toCurrency)
    ) || [];

  return isBuying ? sellRate || null : buyRate || null;
}

export { getAvailableList, findExchangeRate };
