const BASE_CURRENCY = 'PLN';

enum Action {
  SELL = 'SELL',
  BUY = 'BUY',
}

enum RouterPaths {
  SELL = 'sell',
  BUY = 'buy',
  CALC = 'calc',
}

export { BASE_CURRENCY, Action, RouterPaths };
