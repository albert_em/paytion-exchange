import React, { Fragment, ChangeEvent, useState } from 'react';
import { Link } from 'react-router-dom';
import readXlsxFile from 'read-excel-file';

import CssBaseline from '@mui/material/CssBaseline';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import Drawer from '@mui/material/Drawer';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import CloudUploadIcon from '@mui/icons-material/CloudUpload';
import { useTheme } from '@mui/material/styles';
import { useMediaQuery } from '@mui/material';

import { ReactComponent as Paytion } from './assets/paytion.svg';
import { RouterPaths } from './utils/constants';
import { Content } from './components/Content';
import { Footer } from './components/Footer';

export type RatesData = Array<[string, string, number, number]>;

const drawerLinkStyles = {
  display: 'block',
  padding: '15px',
  textDecoration: 'none',
};

const DemoCurrencyConverter = () => {
  const [ratesData, setRatesData] = useState<RatesData | null>(null);
  const [drawerOpen, setDrawerOpen] = useState(false);
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down('sm'));

  const handleFileUpload = async (event: ChangeEvent<HTMLInputElement>) => {
    try {
      const file = event?.target?.files?.[0] as File;
      const rows = await readXlsxFile(file);
      const data = rows.slice(1, 10).map((row) => row.slice(0, 4));
      setRatesData(data as RatesData);
    } catch (error) {
      if (error instanceof Error) {
        alert(error.message);
      }
    }
  };

  const toggleDrawer = (open: boolean) => {
    setDrawerOpen(open);
  };

  const renderLinks = () => (
    <div style={{ display: isMobile ? 'none' : 'flex', marginLeft: 'auto' }}>
      <Link to={RouterPaths.BUY} style={{ marginRight: '10px', textDecoration: 'none' }}>
        <Button color='inherit' sx={{ color: 'white', '&:hover': { color: 'lightgray' } }}>
          Клиент хочет купить
        </Button>
      </Link>
      <Link to={RouterPaths.SELL} style={{ marginRight: '10px', textDecoration: 'none' }}>
        <Button color='inherit' sx={{ color: 'white', '&:hover': { color: 'lightgray' } }}>
          Клиент хочет продать
        </Button>
      </Link>
      <Link to={RouterPaths.CALC} style={{ textDecoration: 'none' }}>
        <Button color='inherit' sx={{ color: 'white', '&:hover': { color: 'lightgray' } }}>
          В злотых под рассчет
        </Button>
      </Link>
    </div>
  );

  const renderDrawerLinks = () => (
    <div role='presentation' onClick={() => toggleDrawer(false)} onKeyDown={() => toggleDrawer(false)} style={{ width: '200px' }}>
      <Link to={RouterPaths.BUY} style={{ ...drawerLinkStyles, color: theme.palette.common.black }}>
        Клиент хочет купить
      </Link>
      <Link to={RouterPaths.SELL} style={{ ...drawerLinkStyles, color: theme.palette.common.black }}>
        Клиент хочет продать
      </Link>
      <Link to={RouterPaths.CALC} style={{ ...drawerLinkStyles, color: theme.palette.common.black }}>
        В злотых под рассчет
      </Link>
    </div>
  );

  return (
    <Fragment>
      <CssBaseline />
      <div style={{ display: 'flex', flexDirection: 'column', minHeight: '100vh' }}>
        <AppBar position='static' sx={{ background: '#333' }}>
          <Toolbar>
            {isMobile && (
              <IconButton size='large' edge='start' color='inherit' aria-label='menu' sx={{ mr: 2 }} onClick={() => toggleDrawer(true)}>
                <MenuIcon />
              </IconButton>
            )}
            <Paytion />
            <Typography variant='h4' ml={2} component='div' sx={{ flexGrow: 1, color: theme.palette.common.white }}>
              Paytion
            </Typography>
            <input type='file' accept='.xlsx' style={{ display: 'none' }} id='file-upload' onChange={handleFileUpload} />
            {ratesData ? renderLinks() : null}
            <label htmlFor='file-upload' style={{ marginLeft: 10 }}>
              <Button variant='contained' component='span' startIcon={<CloudUploadIcon />} color='primary'>
                Upload File
              </Button>
            </label>
          </Toolbar>
        </AppBar>
        {isMobile ? (
          <Drawer anchor='left' open={drawerOpen} onClose={() => toggleDrawer(false)} sx={{ backgroundColor: theme.palette.primary.main }}>
            {renderDrawerLinks()}
          </Drawer>
        ) : null}
        <Content {...{ theme, ratesData, handleFileUpload }} />
        <div style={{ flex: 1 }} /> {/* This will push the Footer to the bottom */}
        <Footer />
      </div>
    </Fragment>
  );
};

export default DemoCurrencyConverter;
