import { Typography } from '@mui/material';

export const Footer = () => {
  const currentYear = new Date().getFullYear();

  return (
    <Typography variant='body2' color='textSecondary' align='center' sx={{ marginTop: 2, padding: 2, backgroundColor: '#333', color: '#fff' }}>
      © {currentYear} Paytion. All rights reserved.
    </Typography>
  );
};
