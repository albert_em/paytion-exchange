import { Card, CardContent, Typography } from '@mui/material';
import ErrorOutlineIcon from '@mui/icons-material/ErrorOutline';
import { useTheme } from '@mui/material/styles';
import { memo } from 'react';

interface ErrorCardProps {
  message?: string;
}

export const ErrorCard = memo<ErrorCardProps>(({ message = 'There is no such page' }) => {
  const theme = useTheme();
  return (
    <Card>
      <CardContent style={{ display: 'flex', flexDirection: 'column', alignItems: 'center', justifyContent: 'center', minHeight: '120px' }}>
        <ErrorOutlineIcon color='error' fontSize='large' style={{ marginBottom: '10px' }} />
        <Typography variant='h6' style={{ color: theme.palette.error.main }}>
          {message}
        </Typography>
      </CardContent>
    </Card>
  );
});
