import { memo } from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import CloudDownloadIcon from '@mui/icons-material/CloudDownload';
import { Theme } from '@mui/material/styles';

interface DownloadCardProps {
  theme: Theme;
}

export const DownloadCard = memo<DownloadCardProps>(({ theme }) => {
  return (
    <Card>
      <CardContent style={{ display: 'flex', flexDirection: 'column', alignItems: 'center', justifyContent: 'center', minHeight: '120px' }}>
        <CloudDownloadIcon color='primary' fontSize='large' style={{ marginBottom: '10px' }} />
        <Typography variant='h6' style={{ color: theme.palette.common.black }}>
          Before you proceed, please download the [.xlsx] file.
        </Typography>
      </CardContent>
    </Card>
  );
});
