import { memo, useState, useMemo, useEffect, ChangeEvent } from 'react';
import { useOutletContext } from 'react-router-dom';
import { Card, CardContent, Typography, Button, Select, MenuItem, FormControl, InputLabel, Grid, SelectChangeEvent, TextField } from '@mui/material';
import { findExchangeRate } from '../utils/helpers';
import { BASE_CURRENCY } from '../utils/constants';
import { RatesData } from '../App';
import { getCurrencyIcon } from '../shared/helpers/getCurrencyIcon';

export const Calc = memo(() => {
  const ratesData = useOutletContext<RatesData>();
  const [fromCurrency, setFromCurrency] = useState<string>(BASE_CURRENCY);
  const [amount, setAmount] = useState<number>(0);
  const [toCurrency, setToCurrency] = useState<string>('');
  const [result, setResult] = useState<number | null>(null);

  const currencyList = [BASE_CURRENCY];

  const givenList = useMemo(() => {
    return ratesData.reduce<{ [key: string]: string[] }>((acc, [baseCurrency, targetCurrency]) => {
      acc[targetCurrency] = [...new Set([...(acc[targetCurrency] || []), baseCurrency])];
      return acc;
    }, {});
  }, [ratesData]);

  const listToGive = useMemo<string[]>(() => givenList[fromCurrency], [givenList, fromCurrency]);

  useEffect(() => {
    if (fromCurrency) {
      setToCurrency(givenList[fromCurrency][0]);
    }
  }, [fromCurrency, givenList]);

  const handleCalculate = () => {
    const buyRate = findExchangeRate(ratesData, fromCurrency, toCurrency, true);

    if (!isNaN(amount) && buyRate !== null) {
      const calculatedResult = amount / buyRate;
      setResult(calculatedResult);
    } else {
      setResult(null);
    }
  };

  const onChangeAmount = (event: ChangeEvent<HTMLInputElement>) => {
    const inputValue = event.target.value;
    const parsedValue = parseFloat(inputValue);

    if (!isNaN(parsedValue)) {
      setAmount(parsedValue);
    } else {
      setAmount(0);
    }
  };

  const handleFromCurrencyChange = (event: SelectChangeEvent<string>) => {
    setFromCurrency(event.target.value as string);
  };

  const handleToCurrencyChange = (event: SelectChangeEvent<string>) => {
    setToCurrency(event.target.value as string);
  };

  return (
    <Card style={{ margin: '20px' }}>
      <CardContent>
        <Typography variant='h6' mb={2}>
          Клиент хочет продать
        </Typography>
        <Grid container spacing={2}>
          <Grid item xs={12} md={4}>
            <FormControl fullWidth>
              <TextField type='number' label='Количество' value={isNaN(amount) ? '' : amount.toString()} onChange={onChangeAmount} />
            </FormControl>
          </Grid>
          <Grid item xs={12} md={4}>
            <FormControl fullWidth>
              <InputLabel>Хочет продать</InputLabel>
              <Select value={fromCurrency} onChange={handleFromCurrencyChange}>
                {currencyList.map((cur) => (
                  <MenuItem key={cur} value={cur}>
                    {getCurrencyIcon(cur)} {cur}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
          </Grid>
          <Grid item xs={12} md={4}>
            {fromCurrency ? (
              <FormControl fullWidth>
                <InputLabel>Просит выдачу в</InputLabel>
                <Select value={toCurrency} onChange={handleToCurrencyChange}>
                  {listToGive.map((cur) => (
                    <MenuItem key={cur} value={cur}>
                      {getCurrencyIcon(cur)} {cur}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>
            ) : null}
          </Grid>
        </Grid>
        <Button onClick={handleCalculate} disabled={!fromCurrency || !toCurrency} variant='contained' color='primary' fullWidth style={{ marginTop: '20px' }}>
          Посчитать
        </Button>
        {result !== null && (
          <Typography variant='h6' sx={{ textAlign: 'center', mt: 2 }}>
            {amount} {fromCurrency} = {result.toFixed(2)} {toCurrency}
          </Typography>
        )}
      </CardContent>
    </Card>
  );
});
