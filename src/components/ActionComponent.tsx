/* eslint-disable @typescript-eslint/no-unused-vars */
import { memo, useState, useMemo, useEffect, ChangeEvent } from 'react';
import { useOutletContext } from 'react-router-dom';
import { Card, CardContent, Typography, Button, Select, MenuItem, InputLabel, FormControl, TextField } from '@mui/material';

import { RatesData } from '../App';
import { findExchangeRate, getAvailableList } from '../utils/helpers';
import { Action } from '../utils/constants';
import { getCurrencyIcon } from '../shared/helpers/getCurrencyIcon';

interface ActionComponentProps {
  type: Action;
}

export const ActionComponent = memo<ActionComponentProps>(({ type }) => {
  const ratesData = useOutletContext<RatesData>();
  const [fromCurrency, setFromCurrency] = useState<string>('');
  const [amount, setAmount] = useState<number>(0);
  const [toCurrency, setToCurrency] = useState<string>('');
  const [result, setResult] = useState<number | null>(null);

  const isBuying = type === Action.BUY;

  const currencyList = useMemo<string[]>(() => getAvailableList(ratesData), [ratesData]);

  const givenList = useMemo(
    () =>
      currencyList.reduce<Record<string, string[]>>((acc, currency) => {
        const relatedCurrencies = ratesData.filter(([baseCurrency]) => baseCurrency === currency).map(([_, counterCurrency]) => counterCurrency);

        acc[currency] = relatedCurrencies;
        return acc;
      }, {}),
    [currencyList, ratesData]
  );

  const listToGive = useMemo<string[]>(() => givenList[fromCurrency], [givenList, fromCurrency]);

  const handleCalculate = () => {
    const buyRate = findExchangeRate(ratesData, fromCurrency, toCurrency, isBuying);

    if (!isNaN(amount) && buyRate !== null) {
      const calculatedResult = amount * buyRate;
      setResult(calculatedResult);
    } else {
      // Handle error when amount is not a valid number or rates are not available
      setResult(null);
    }
  };

  const onChangeAmount = (event: ChangeEvent<HTMLInputElement>) => {
    const inputValue = event.target.value;
    const parsedValue = parseFloat(inputValue);

    if (!isNaN(parsedValue)) {
      setAmount(parsedValue);
    } else {
      setAmount(0);
    }
  };

  useEffect(() => {
    if (fromCurrency) {
      setToCurrency(givenList[fromCurrency][0]);
    }
  }, [fromCurrency, givenList]);

  useEffect(() => {
    setResult(null);
  }, [fromCurrency, toCurrency, amount]);

  return (
    <Card sx={{ maxWidth: 400, margin: 'auto', marginTop: 20 }}>
      <CardContent>
        <Typography variant='h5' component='div' mb={2}>
          {isBuying ? 'Клиент хочет купить' : 'Клиент хочет продать'}
        </Typography>
        <TextField type='number' label='Количество' value={isNaN(amount) ? '' : amount.toString()} onChange={onChangeAmount} fullWidth sx={{ mb: 2 }} />
        <FormControl fullWidth sx={{ mb: 2 }}>
          <InputLabel htmlFor='from-currency' sx={{ paddingRight: 1 }}>
            Валюта
          </InputLabel>
          <Select id='from-currency' value={fromCurrency} onChange={(e) => setFromCurrency(e.target.value)} fullWidth>
            {currencyList.map((cur) => (
              <MenuItem key={cur} value={cur}>
                {getCurrencyIcon(cur)} {cur}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
        {fromCurrency ? (
          <>
            <FormControl fullWidth sx={{ mb: 2 }}>
              <InputLabel htmlFor='to-currency' sx={{ paddingRight: 1 }}>
                {isBuying ? ' Дает деньги в' : 'Просит выдачу в'}
              </InputLabel>
              <Select id='to-currency' value={toCurrency} onChange={(e) => setToCurrency(e.target.value)} fullWidth>
                {listToGive.map((cur) => (
                  <MenuItem key={cur} value={cur}>
                    {getCurrencyIcon(cur)} {cur}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
          </>
        ) : null}
        <Button onClick={handleCalculate} disabled={!fromCurrency || !toCurrency || !amount} fullWidth variant='contained' color='primary' sx={{ mb: 2 }}>
          Посчитать
        </Button>
        {result !== null && (
          <Typography variant='h6' color='primary' sx={{ textAlign: 'center' }}>
            {amount} {fromCurrency} = {result.toFixed(2)} {toCurrency}
          </Typography>
        )}
      </CardContent>
    </Card>
  );
});
