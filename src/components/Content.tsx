import { FC } from 'react';
import { Outlet } from 'react-router-dom';

import { Theme } from '@mui/material/styles';
import Container from '@mui/material/Container';
import Grid from '@mui/material/Grid';

import { RatesData } from '../App';
import { DownloadCard } from './DownloadCard';

interface ContentProps {
  ratesData: RatesData | null;
  theme: Theme;
}

export const Content: FC<ContentProps> = ({ ratesData, theme }) => {
  return (
    <Container component='main' maxWidth='md' sx={{ display: 'flex', flexDirection: 'column', alignItems: 'center', mt: 4 }}>
      {ratesData ? (
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <Outlet context={ratesData} />
          </Grid>
        </Grid>
      ) : (
        <DownloadCard theme={theme} />
      )}
    </Container>
  );
};
