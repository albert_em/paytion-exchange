import { createBrowserRouter } from 'react-router-dom';

import { Calc } from '../../components/Calc';
import { ActionComponent } from '../../components/ActionComponent';
import { ErrorCard } from '../../components/ErrorCard';

import { Action, RouterPaths } from '../../utils/constants';

import App from '../../App';

export const router = createBrowserRouter([
  {
    path: '/',
    element: <App />,
    errorElement: <ErrorCard />,
    children: [
      {
        path: RouterPaths.BUY,
        element: <ActionComponent key={Action.BUY} type={Action.BUY} />,
      },
      {
        path: RouterPaths.SELL,
        element: <ActionComponent key={Action.SELL} type={Action.SELL} />,
      },
      {
        path: RouterPaths.CALC,
        element: <Calc />,
      },
    ],
  },
]);
