export function getCurrencyIcon(currencyCode: string): string {
  switch (currencyCode) {
    case 'USD':
      return '🇺🇸';
    case 'EUR':
      return '🇪🇺';
    case 'UAH':
      return '🇺🇦';
    case 'GBP':
      return '🇬🇧';
    case 'CHF':
      return '🇨🇭';
    case 'USDT':
      return '💵';
    case 'PLN':
      return '🇵🇱';
    default:
      return '';
  }
}
