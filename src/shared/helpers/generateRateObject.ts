import { Row } from 'read-excel-file';

export const generateRateObject = (data: Row[]): Record<string, number> => {
  const rateObject: Record<string, number> = {};

  data.forEach(([fromCurrency, toCurrency, buyRate, sellRate]) => {
    const pair = `${fromCurrency}${toCurrency}`.toUpperCase();

    rateObject[pair] = buyRate as number;
    rateObject[`${toCurrency}${fromCurrency}`.toUpperCase()] = sellRate as number;
  });

  return rateObject;
};
