export const getAllCurrenciesFromPairs = (currencyPairs: { [key: string]: number }): string[] => {
  const currencies: Set<string> = new Set();

  for (const pair in currencyPairs) {
    const fromCurrency = pair.slice(0, 3);
    const toCurrency = pair.slice(3);

    currencies.add(fromCurrency);
    currencies.add(toCurrency);
  }

  return Array.from(currencies);
};
